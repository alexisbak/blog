# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->            
	$('.close').click ->
		clearModal()

	clearModal = ->
		$('#qty').val(1)	
		$('.error_wrapper').hide()
		$(".ch_extra").each ->
			$(this).attr "checked", false
		
	$('.modal #qty').change ->
		if $(this).val() > 0
			$('.error_wrapper').hide()
		else 
			$('.error_wrapper').show()
	
	$('.btn_addtocart').click ->
		mymodal = $(this).parents('.modal')		
		hidden_div = mymodal.find('#hidden_values')
		detail_id = hidden_div.find('.detail_id').text()
		detail_name = hidden_div.find('.detail_name').text()
		detail_img = hidden_div.find('.detail_img').text()
		detail_size = hidden_div.find('.detail_size').text()
		detail_price = hidden_div.find('.detail_price').text()
		
	
		if parseInt(mymodal.find('#qty').val()) > 0
			mycookie = getCookie('cart')
			cart = []
			new_item = {}
			new_item['id'] = detail_id
			qty = mymodal.find('#qty').val()
			new_item['qty'] = qty
			extras = []						
			ext_div = ""
			exists = false
			cart_table = $('#cart_dropdown')
			total = 0	
			n = 0		
			
			mymodal.find(".ch_extra").each ->
				if $(this).is(':checked')
					extras.push($(this).attr('data-id')) 
					ext_price = $.trim($(this).parents('.checkbox').find('.ext_price').text())
					ext_price = ext_price.slice(ext_price.indexOf("$") + 1, ext_price.length)
					ext_name = $(this).parents('.checkbox').find('.ext_name').text()
					ext_div += '<div class="ext" data-price=' + ext_price + '>' + ext_name + '</div>'
				
			new_item['extra'] = JSON.stringify(extras)
			if mycookie
				cart = JSON.parse(decodeURIComponent(mycookie))
				cart.push(new_item) if addItem(cart, new_item) != true												
			else 			
				cart.push(new_item)

			setCookie('cart', JSON.stringify(cart), 1)				
					
			cart_table.find('.item').each ->
				if $(this).attr('data-id') == detail_id
					old_qty = $.trim($(this).find('.qty').text())
					old_qty = old_qty.slice(old_qty.indexOf("Qty") + 4, old_qty.length)
					old_qty = parseInt(old_qty)
					new_qty = old_qty + parseInt(qty)
					$(this).find('.qty').text("Qty " + new_qty)
					exists = true			
					$(this).find('.extra').html(ext_div)					
			
			if !exists 								
				$('<tr class="item" data-id="' + new_item['id'] + '"></tr>').insertBefore('#cart_dropdown tr:nth-last-child(2)')
				$('<td><div class="img_wrapper"><img src="' + detail_img + '"/></div></td>').appendTo('#cart_dropdown tr:nth-last-child(3)')
				td_detail = '<td>'
				td_detail += '<button type="button" class="remove" style="border: 1px solid lightgrey; float: right">&times;</button>'
				td_detail += '<div class="price">' + detail_price + '</div>'
				td_detail += '<div class="title">' + detail_name + '</div>'
				td_detail += '<div class="size">(' + detail_size + ')</div>'
				td_detail += '<div class="qty">Qty ' + qty + '</div>'
				td_detail += '<div class="extra">'
				td_detail += ext_div
				td_detail += '</div>'
				td_detail += '</td>'				
				$('#cart_dropdown tr:nth-last-child(3)').append($(td_detail))				
			
			tax = parseFloat(cart_table.find('.tax').text())
			del_fee = parseFloat(cart_table.find('.del_fee').text())
			
			cart_table.find('.item').each ->
				price = $.trim($(this).find('.price').text())
				price = price.slice(price.indexOf("$") + 1, price.length)
				price = parseFloat(price)
				qty = $.trim($(this).find('.qty').text())
				qty = qty.slice(qty.indexOf("Qty") + 4, qty.length)
				qty = parseInt(qty)
				n += qty
				ext_total = 0
				$(this).find('.extra .ext').each ->
					ext_total += parseFloat($(this).attr('data-price'))
				total += price*qty					
				total += ext_total*qty
			
			total += (total * tax) / 100
			total += del_fee;
			
			cart_table.find('.total:last').text('$' + total.toFixed(2))
			$('.mycart #total').text(total.toFixed(2))
			$('.mycart #n').text(n)
			$('.mycart').attr('data-n', n)		
																										
			clearModal()		
			$('#checker').text("true")
			mymodal.modal('toggle')				
												
		else
			$('.error_wrapper').show()
				
	addItem = (cart, new_item) ->
		if cart.length > 0
			for i in [0..cart.length-1]
				if(cart[i]['id'] is new_item['id'])
					cart[i]['qty'] = parseInt(cart[i]['qty']) + parseInt(new_item['qty'])
					cart[i]['extra'] = new_item['extra']
					return true
	
			
	setCookie = (name, value, days) ->
	  if days
	    date = new Date()
	    date.setTime date.getTime() + (days * 24 * 60 * 60 * 1000)
	    expires = "; expires=" + date.toGMTString()
	  else
	    expires = ""
	  document.cookie = name + "=" + encodeURIComponent(value) + expires + "; path=/"
	
	getCookie = (name) ->
	  nameEQ = name + "="
	  ca = document.cookie.split(";")
	  i = 0
	 
	  while i < ca.length
	    c = ca[i]
	    c = c.substring(1, c.length)  while c.charAt(0) is " "
	    return c.substring(nameEQ.length, c.length)  if c.indexOf(nameEQ) is 0
	    i++
	  null

	deleteCookie = (name) ->
	  setCookie name, "", -1



	