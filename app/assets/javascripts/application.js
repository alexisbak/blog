// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require fancybox
//= require jquery_ujs
//= require bootstrap
//= require jquery.validate
//= require jquery.validate.additional-methods
//= require s3_direct_upload
//= require underscore
//= require gmaps/google
//= require_tree .
jQuery(document).ready(function() {

	set_size_all();
	set_select();
	paginate(true);
	$('.pg_wrapper li').click(handler);
	$('.cart_link .remove').click(remove);

	$(window).resize(function() {
		set_size_all();
	});

	if ($('#map_canvas').length > 0)
		create_map();
	function create_map() {

		handler = Gmaps.build('Google');
		handler.buildMap({
			provider : {
				disableDefaultUI : true
			},
			internal : {
				id : 'map_canvas'
			}
		}, function() {
			markers = handler.addMarkers([{
				"lat" : 34.056701,
				"lng" : -118.25037609999998,
				"infowindow" : "PrimeGrind Coffee and Crepe House"
			}]);

			handler.bounds.extendWith(markers);
			handler.fitMapToBounds();
			handler.getMap().setZoom(16);
		});
	}

	function scroll_to_content() {
		var home = $('.top_wrapper a:first');

		if (home.attr('class') === "menu_active") {

		} else {
			$("body, html").animate({
				scrollTop : $('.content').offset().top
			}, 400);
		}
	}


	$('.size_wrapper select').change(function() {
		var parent = $(this).parents('.item');
		var option = $(this).find("option:selected");
		parent.find('.price_wrapper').text('$' + option.val());
		parent.find('.glyphicon-info-sign').hide();
	});

	$('.mycart').hover(function() {
		var n_cart = $(this).attr('data-n');
		if (n_cart > 0) {
			$('.cart_link .dropdown').trigger('show.bs.dropdown');
		}
	});

	$('.cart_dropdown').hover(function() {

	}, function() {
		$('.cart_link .dropdown').trigger('hide.bs.dropdown');
	});

	$('.modal').find('.close').click(function() {
		$('.modal').modal('hide');
	});

	$('.cart_modal').on('hidden.bs.modal', function() {
		if ($('#checker').text() === "true") {
			$('#checker').text("");
			var id = $(this).find(".detail_id").text();
			$.each($('.cart_link .dropdown .item'), function(i, item) {
				if ($(item).attr('data-id') == id) {
					$(item).find('button').click(remove);
				}
			});
			paginate(false);
			$('.cart_link .dropdown').trigger('show.bs.dropdown');
			setTimeout(function() {
				$('.cart_link .dropdown').trigger('hide.bs.dropdown');
			}, 2000);
		}
	});

	$('.cart_modal').click(function() {
		var parent = $(this).parents('.item');
		var option = parent.find('select option:selected');
		if (option.text() !== "select size") {
			var modal_name = $(this).attr('data-modal');
			var cur_modal = $('#' + modal_name);
			cur_modal.find('.modal-title').text(parent.find('.title').text());
			cur_modal.find('.modal-title').append('<label>(' + option.text() + ')</label>');
			cur_modal.find('.detail_id').text(option.attr('data-id'));
			cur_modal.find('.detail_img').text(parent.find('.btn_img_detail').attr('data-img'));
			cur_modal.find('.detail_price').text(parent.find('.price_wrapper').text());
			cur_modal.find('.detail_size').text(option.text());
			cur_modal.find('.detail_name').text(parent.find('.title').text());

			cur_modal.modal('toggle');
		} else {
			parent.find('.glyphicon-info-sign').show();
		}
	});

	$('.right_top_wrapper .dropdown').on('show.bs.dropdown', function(e) {
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown();
	});

	$('.right_top_wrapper .dropdown').on('hide.bs.dropdown', function(e) {
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp();
	});

	$('.cart_link .dropdown').on('show.bs.dropdown', function(e) {
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown(800);
	});

	$('.cart_link .dropdown').on('hide.bs.dropdown', function(e) {
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp(800);
	});

	function set_select() {
		var items = $('.cat_wrapper .item');
		$.each(items, function(index, item) {
			$(item).find('select option:first').prop('selected', true)
		});
	}

	function handler(event) {
		event.preventDefault();

		$.each($('.pg_wrapper li'), function(index, link) {
			$(link).removeClass('active');
		});

		$(this).addClass('active');
		var counter = $(this).find('a').attr('data-id');
		var length = counter * 3;
		var items = $('#cart_dropdown').find('.item');

		if (length > items.length) {

			length = items.length;
		}

		$.each(items, function(index, item) {
			$(item).hide();
		});

		for (var i = (counter - 1) * 3; i < length; i++) {
			$(items[i]).show();
		}
	};

	function paginate(first) {

		var items = $('#cart_dropdown .item');
		if (items.length > 3) {
			$('.pg_wrapper').show();
			var counter = 1;
			var i = 0;

			$('.pg_wrapper .pagination').html("");
			while (i < items.length) {
				$('.pg_wrapper .pagination').append(function() {
					return $('<li><a href="#" data-id="' + counter + '">' + counter + '</a></li>').click(handler);
				})
				i = i + 3;
				counter++;
			}

			if (first) {
				$('.pg_wrapper .pagination li:first').addClass('active');
				$.each(items, function(index, item) {
					$(item).show();
					if (index > 2)
						$(item).hide();

				});
			} else {
				$.each($('.pg_wrapper li'), function(index, link) {
					$(link).removeClass('active');
				});

				$('.pg_wrapper li:last').addClass('active');
				var last_id = $('.pg_wrapper li:last').find('a').attr('data-id');
				$.each(items, function(index, item) {
					$(item).show();
					if (index < (last_id - 1) * 3)
						$(item).hide();

				});
			}

		} else {
			$('.pg_wrapper').hide();
			items.show();
		}
	}

	function calculate_total_2() {
		var table = $('#cart_dropdown');
		var total = 0;
		var tax = parseFloat(table.find('.tax').text());
		var del_fee = parseFloat(table.find('.del_fee').text());

		$.each(table.find('.item'), function(index, item) {
			var price = $.trim($(item).find('.price').text());
			price = price.slice(price.indexOf("$") + 1, price.length);
			price = parseFloat(price);
			var qty = $.trim($(item).find('.qty').text());
			qty = qty.slice(qty.indexOf("Qty") + 4, qty.length);
			qty = parseInt(qty);

			total += price * qty;

			var ext_total = 0;

			var extras = $(item).find('.extra .ext');
			$.each(extras, function(i, ext) {

				var ext_price = parseFloat($(ext).attr("data-price"));
				ext_total += ext_price;
			});
			total += ext_total * qty

		});
		total += (total * tax) / 100;
		total += del_fee;
		table.find('.total:last').text(total.toFixed(2));
		return total;
	}

	function set_size_all() {

		var cart_link_width = $('.cart_link').width();

		$('.cart_link .dropdown-menu').css({
			left : cart_link_width - 340 + "px"
		});

		var window_width = $(window).width();
		var social = $('.social');
		var menu = $('.menu');
		var right = $('.right_side');
		var top_wrapper = $('.top_wrapper');
		var title_wrapper = $('.title_wrapper');
		var account = $(".account");
		var header = $('#header_td');
		var layer_top = $('.layer_top');
		var layer_bottom = $('.layer_bottom');
		var caption = $('.header .caption');
		var search = $('.search_wrapper');
		console.log(window_width);

		if (window_width < 1060) {
			social.css({
				top : '30px'
			});
		} else {
			social.css({
				top : '10px'
			});
		}

		if (window_width < 900) {
			menu.hide();
			layer_top.hide();
			layer_bottom.hide();
			header.removeClass('padding_top');
			header.addClass('no_padding_top');
			title_wrapper.removeClass('padding_left');
			title_wrapper.addClass('center');
			title_wrapper.addClass('no_padding_left');
			caption.removeClass('margin_right');
			caption.addClass('no_margin_right');
			search.removeClass('absolute_right');
			search.addClass('absolute_left');

		} else {
			menu.show();
			layer_top.show();
			layer_bottom.show();
			header.removeClass('no_padding_top');
			header.addClass('padding_top');
			title_wrapper.removeClass('no_padding_left');
			title_wrapper.addClass('padding_left');
			title_wrapper.removeClass('center');
			caption.removeClass('no_margin_right');
			caption.addClass('margin_right');
			search.removeClass('absolute_left');
			search.addClass('absolute_right');
		}

	}

	function remove() {
		var id = $(this).parents(".item").attr("data-id");

		var items = $('#cart_dropdown .item');
		$.each(items, function(index, item) {
			if ($(item).attr('data-id') == id) {
				$(item).children('td').animate({
					padding : 0
				}).wrapInner('<div />').children().slideUp(function() {
					$(this).closest('tr').remove();
					paginate(true);

					var cart_rows = $('#div_cart .glyphicon-remove');

					if (cart_rows.length > 0) {
						$.each(cart_rows, function(index, row) {
							if ($(row).attr("data-id") == id) {
								$(row).trigger("click");
							}
						});

					} else {
						var mycookie = getCookie('cart');
						var cart = JSON.parse(decodeURIComponent(mycookie));
						var i = 0;
						var total = 0;
						while (i < cart.length) {
							if (cart[i]['id'] == id) {
								cart.splice($.inArray(cart[i], cart), 1);
								setCookie('cart', JSON.stringify(cart), 1);
							}
							i++;
						}
						$.each(cart, function(j, item) {
							total += parseInt(item['qty']);
						});
						var total_cost = 0;
						if (cart.length == 0) {
							$('#div_cart').text("Cart is empty.");
						} else {
							total_cost = calculate_total_2();
						}
						$('.mycart #total').text(total_cost.toFixed(2));
						$('.mycart #n').text(total);
						$('.mycart').attr('data-n', total);
						$('.cart_link .total:last').text('$' + total_cost.toFixed(2));
					}
				});

			}
		});
	}


	$('.glyphicon-remove').click(function() {
		var id = $(this).attr('data-id');

		var items = $('#cart_dropdown .item');
		$.each(items, function(index, item) {
			if ($(item).attr('data-id') == id) {
				$(item).detach();
				paginate(true);
			}
		});

		var mycookie = getCookie('cart');
		var cart = JSON.parse(decodeURIComponent(mycookie));
		var i = 0;
		while (i < cart.length) {
			if (cart[i]['id'] == id) {

				cart.splice($.inArray(cart[i], cart), 1);
				setCookie('cart', JSON.stringify(cart), 1);

				$(this).parents('tr').children('td').animate({
					padding : 0
				}).wrapInner('<div />').children().slideUp(function() {
					var total = 0;
					$(this).closest('tr').remove();
					$.each(cart, function(j, item) {
						total += parseInt(item['qty']);
					});
					var total_cost = 0;
					if (cart.length == 0) {
						$('#div_cart').text("Cart is empty.");
					} else {
						total_cost = calculate_total();
					}
					$('.mycart #total').text(total_cost.toFixed(2));
					$('.mycart #n').text(total);
					$('.mycart').attr('data-n', total);
					$('.cart_link .total:last').text('$' + total_cost.toFixed(2));
				});
			}
			i++;
		}

	});

	function calculate_total() {
		var items = $('#tbl_cart').find('tr:not(:first)').not("tr tr");
		var subtotal = 0;
		var total = 0;
		var tbl_calc = $('#table_calc');

		$.each(items, function(i, item) {
			var price_str = $.trim($(item).find('.price').text());
			var price = parseFloat(price_str.substring(1, price_str.length))
			var qty = parseInt($(item).find('.qty').text());
			subtotal += qty * price;

			var $extra_tr = $(item).find('table').find('tr');
			var ext_total = 0;
			$.each($extra_tr, function(i, tr) {
				var $chbox = $(tr).find('input');
				if ($chbox.is(":checked")) {
					var ext_price = parseFloat($(tr).find('input').val());
					ext_total += ext_price;
				}
			});
			subtotal += ext_total * qty
		});

		var tax = (subtotal * parseFloat(tbl_calc.find('.cart_tax').attr('data-tax'))) / 100;
		var del_str = tbl_calc.find('.cart_fee').text();
		var del_fee = parseFloat($.trim(del_str).substring(1, del_str.length));

		total = subtotal + tax + del_fee;

		tbl_calc.find('.cart_subtotal').text('$' + subtotal.toFixed(2));
		tbl_calc.find('.cart_tax').text('$' + tax.toFixed(2))
		tbl_calc.find('.cart_total').text('$' + total.toFixed(2))

		return total;
	}

	function setCookie(name, value, days) {
		var expires = "";
		if (days != null)
			var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toGMTString();

		document.cookie = name + "=" + encodeURIComponent(value) + expires + "; path=/";
	}

	function getCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(";")
		var i = 0;

		while (i < ca.length) {
			var c = ca[i];
			while (c.charAt(0) == " ") {
				c = c.substring(1, c.length);
			}
			if (c.indexOf(nameEQ) == 0)
				return c.substring(nameEQ.length, c.length);
			i++;
		}
	}

	function deleteCookie(name) {
		setCookie(name, "", -1);
	}

	var submitted = false;

	$('#form_login').validate({
		rules : {
			email : {
				required : true,
				email : true
			},
			password : {
				required : true,
				minlength : 6
			}
		},
		showErrors : function(errorMap, errorList) {

			if (submitted) {

				var summary = "";

				$.each(errorMap, function(key, value) {

					var name = $('#' + key).parents('.form-group').find('label').text();
					summary += "<li><span style='font-size: 12px'>" + "<strong>" + name + "</strong>" + ' ' + value + "</span></li>";
				});

				var $alert_error = $('.error_inner');
				$alert_error.find('ul').html(summary);
				$('.error_wrapper').show();

				submitted = false;
			}
		},
		invalidHandler : function(form, validator) {

			submitted = true;
			$('.error_wrapper').hide();
		}
	});

	$('.radio_wrapper').click(function(event) {
		var radios = $('.radio_wrapper');

		$.each(radios, function(i, radio) {
			$(radio).removeClass('radio_active');
		});

		$(this).addClass('radio_active');
		$(this).find('input').prop("checked", true)
		if ($(this).find('input').val() === "2") {
			$('.order_wrapper').find('.card_wrapper').show();
		} else {
			$('.order_wrapper').find('.card_wrapper').hide();
		}
	});

	$('.btn_img_detail').click(function(event) {
		var img_modal = $('#imgModal');
		var item = $(this).parents('.item');
		img_modal.find('img').prop('src', $(this).attr('data-img'));
		img_modal.find('.caption').text(item.find('.title').text());
		img_modal.find('.body').text(item.find('.desc').text());
	});
	setDates();

	function setDates() {
		var months = ['Jan', 'Feb', 'March', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		var days = ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
		var d = new Date();
		d.setHours(7);
		d.setMinutes(0);

		var month = months[d.getMonth()];
		var day = d.getDate() + 1;
		var day_of_week = d.getDay();

		$('#order_schedule_date').addClass('form-control');
		$('#order_schedule_date').append('<option value="today" selected>Today</option>');
		for (var i = 0; i < 7; i++) {
			var option = $('<option/>');
			var value = days[day_of_week] + ", " + month + " " + day;
			option.attr({
				'value' : value
			}).text(value);

			$('#order_schedule_date').append(option)
			day++;
			day_of_week++;

			if (day_of_week == 7) {
				day_of_week = 0;
			}
		}

		$('#order_schedule_time').addClass('form-control');
		$('#order_schedule_time').append('<option value="asap" selected>ASAP</option>');

		for (var j = 0; j < 25; j++) {
			var option = $('<option/>');
			var newDateObj = new Date(d.getTime() + j * 30 * 60000);

			var min = newDateObj.getMinutes();
			if (min == 0)
				min = '00';

			var value = newDateObj.getHours() + ':' + min;
			option.attr({
				'value' : value
			}).text(value);

			$('#order_schedule_time').append(option);
		}
	}


	$('#addressModal').find('button').click(function(event) {
		var $sel_option = $('#addressModal').find("select option:selected");

		$('#del_address').text($sel_option.text());
		$('#order_address_id').val($sel_option.val());
		$('#addressModal').modal('hide');
	});

	$('#new_address').click(function(event) {
		var labels = $('#newAddressModal').find('.control-label');
		$.each(labels, function(i, label) {
			$(label).css({
				'color' : '#000',
				'font-weight' : 'normal'
			});
		});

		var inputs = $('#newAddressModal').find('.form-control').parent('div');

		$.each(inputs, function(j, input) {
			$(input).removeClass('col-sm-4');
			$(input).addClass('col-sm-6');
		});

		$('#div_wrap_1').addClass('modal_form');
		$('#div_wrap_2').addClass('modal_form');

	});

	load_order($($('.order_menu')[0]));

	function load_order($menu) {
		$menu.addClass('active_div');
		var id = $menu.attr('data-id');

		$.each($('.order_detail'), function(i, detail) {
			$(detail).hide();
			if ($(detail).attr('data-id') == id) {
				$(detail).show();
			}
		});

	}


	$('.order_menu').click(function(event) {
		var menus = $('.order_menu');

		$.each(menus, function(i, menu) {
			$(menu).removeClass('active_div');

		});

		$(this).addClass('active_div');

		load_order($(this));
	});

	var address_submitted = false;

	$('#new_address_form').validate({
		rules : {
			address1 : {
				required : true
			},
			city : {
				required : true
			},
			zip : {
				required : true
			},
			name : {
				required : true
			},
			phone : {
				required : true
			}
		},
		showErrors : function(errorMap, errorList) {

			if (address_submitted) {

				var summary = "";

				$.each(errorMap, function(key, value) {

					var name = $('#' + key).parents('.form-group').find('label').text();
					summary += "<li><span style='font-size: 12px'>" + "<strong>" + name + "</strong>" + ' ' + value + "</span></li>";
				});

				var $alert_error = $('.error_inner');
				$alert_error.find('ul').html(summary);
				$('.error_wrapper').show();

				submitted = false;
			}
		},
		invalidHandler : function(form, validator) {

			address_submitted = true;
			$('.error_wrapper').hide();
		}
	});

	$('div.checkbox').click(function() {
		var $chbox = $(this).find('input');
		if ($chbox.is(':checked')) {
			$chbox.prop('checked', false);
		} else {
			$chbox.prop('checked', true)
		}
	});

	$('.order_wrapper form').submit(function() {
		scroll_to_content();
		$.fancybox.showActivity();
		$('.order_wrapper').fadeTo('slow', .6);
		$('.order_wrapper').append('<div style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>');
	});
});

