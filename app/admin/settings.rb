ActiveAdmin.register Settings do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  permit_params :value

  form do |f|
      f.inputs "Update Settings" do
      f.input :name, :input_html => {:disabled => true}
      f.input :value
    end
    f.actions
  end

  index do
    column :name, class: 'bold'
    column 'Value' do |set|
      status_tag set.value, class: 'preparing'
    end
    actions defaults: false do |set|
      link_to 'Edit',  edit_admin_setting_path(set), class: 'all_btn'
    end
  end

  show do
    attributes_table do
      row :name
      row :value
    end
    active_admin_comments
  end

end
