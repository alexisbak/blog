ActiveAdmin.register ExtraProduct do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  belongs_to :category
  permit_params :category_id, :name, :price

  form do |f|
    f.inputs "Extra Product" do
      f.input :category, :input_html => {:disabled => true}
      f.input :name
      f.input :price, :type => :decimal
    end
    f.actions
  end

  index do
    column :category do |ext|
      ext.category.name      
    end
    column :name, class: 'bold'
    column 'Price' do |ext|
      b '$' + ext.price.to_s      
    end
    actions
  end
end
