ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do

    columns do
      column do
        orders = Order.where('status_id = ?', 1)
        unless orders.nil?
          panel "New Orders (" + orders.size().to_s + ")" do
            table_for orders do
              column "Order #", :name, class: 'bold'
              column "Scheduled at" do |o|
                o.schedule_date.upcase + ", " + o.schedule_time.upcase
              end
              column "Payment" do |o|
                b o.payment_type.name.upcase
                unless o.payment_type.id == 1
                  div o.transaction_id
                end
                div status_tag o.payment_status
              end
              column (:order_type) do |o|

                if o.order_type_id == 1
                  h5 o.order_type.name, class: 'delivery'
                else
                  h5 o.order_type.name, class: 'pickup'
                end
                div b link_to o.user.name, admin_user_path(o.user)
                if o.order_type.id == 1
                  div do
                    address = o.user.addresses.find_by(:default => true)
                    div address.address1 + ', ' + address.address2
                    div ActiveSupport::NumberHelper.number_to_phone(address.phone, area_code: true)
                  end
                end
              end
              column "Order Status" do |o|
                status_tag o.status.name
              end
              column "Amount" do |o|
                b '$' + o.total.to_s
              end
              column "Order Details" do |o|
                div class: 'bg_white' do
                  o.order_details.each do |detail|
                    p_detail = detail.product_detail
                    size = "one size"
                    unless p_detail.size.nil?
                    size = p_detail.size.name
                    end
                    div link_to p_detail.product.name + " (" + size + ") - Qty" + detail.qty.to_s,  admin_order_order_detail_path(o, detail)
                    ul do
                      detail.order_extras.each do |ext|
                        li ext.extra_product.name, class: 'style_none'
                      end
                    end
                  end
                end
              end
              column '' do |order|
                div class: 'b_wrapper' do
                  link_to 'Update',  edit_admin_order_path(order), class: 'all_btn'
                end
              end
            end
          end

        end
      end
    end

    div class: "blank_slate_container", id: "dashboard_default_message" do
      span class: "blank_slate" do
        span I18n.t("active_admin.dashboard_welcome.welcome")
        small I18n.t("active_admin.dashboard_welcome.call_to_action")
      end
    end
  # Here is an example of a simple dashboard with columns and panels.
  #
  # columns do
  #   column do
  #     panel "Recent Posts" do
  #       ul do
  #         Post.recent(5).map do |post|
  #           li link_to(post.title, admin_post_path(post))
  #         end
  #       end
  #     end
  #   end

  #   column do
  #     panel "Info" do
  #       para "Welcome to ActiveAdmin."
  #     end
  #   end
  # end
  end # content
end
