ActiveAdmin.register Status do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
menu parent: "Order"
permit_params :name

index do
  column 'Status Name' do |status|
    status_tag status.name
  end
end

end
