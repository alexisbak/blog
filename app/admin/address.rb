ActiveAdmin.register Address do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  belongs_to :user
  permit_params :address1, :address2, :city, :zip, :default, :user_id, :name, :phone

  index do
    column :name
    column :address1
    column :address2
    column :city
    column :zip
    column :phone
    column :default
  end

end
