ActiveAdmin.register OrderDetail do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  belongs_to :order

  permit_params :order_id, :product_detail_id, :qty, :product_id, :size_id, :size

  show do
    div class: 'border' do
      h3 b 'Order # ' + order_detail.order.name
      product_detail = order_detail.product_detail
      size = "one size".upcase
      unless product_detail.size.nil?
      size = product_detail.size.name.upcase
      end
      div class: 'medium' do
        div product_detail.product.name + " (" + size + ")"
        div b "$" + product_detail.price.to_s
        div 'Qty ' + order_detail.qty.to_s, class: 'medium'
        ul do
          order_detail.order_extras.each do|ext|
            li ext.extra_product.name + " - $" + ext.extra_product.price.to_s, class: 'style_none'
          end
        end
      end

    end
  end

  index do
    column 'Order #' do |detail|
      '#' + detail.order.name
    end
    column 'Product' do |detail|
      b detail.product_detail.product.name
      detail.order_extras.each do |ext|
        div ext.extra_product.name + " - $" + ext.extra_product.price.to_s, class: 'small'
      end
    end
    column 'Size' do |detail|
      unless detail.product_detail.size.nil?
      detail.product_detail.size.name.upcase
      else
        div 'one size'.upcase, class: 'small'
      end

    end
    column 'Price' do |detail|
      b '$' + detail.product_detail.price.to_s
    end
    column :qty
  end

end
