ActiveAdmin.register ProductDetail do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  belongs_to :product
  permit_params :category_id, :product_id, :size_id, :price

  form do |f|
    cat_id = Product.find(params[:product_id]).category_id
    f.inputs "Product Detail" do
      f.input :category_id, :input_html => { :value => cat_id }, :as => :hidden
      f.input :product, :input_html => {:disabled => true}
      f.input :size, :as => :select, :collection => Size.select(:id, :name).where("category_id = ?", cat_id)
      f.input :price, :type => :decimal
    end
    f.actions
  end

  index do
    selectable_column
    column :product do |detail|
      b detail.product.name
    end
    column 'Size' do |detail|
      unless detail.size.nil?
      detail.size.name
      else
        'one size'
      end
    end
    column 'Price' do |detail|
      b '$' + detail.price.to_s
    end
    actions
  end

end
