ActiveAdmin.register Category do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  menu parent: "Product"
  permit_params :name, :image

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Product" do
      f.input :name
    end
    f.actions
  end

  index do
    column :name, class: 'bold'
    column :size do |category|
      link_to 'Sizes', admin_category_sizes_path(category)
    end
    column :extra do |category|
      link_to 'Extra Items', admin_category_extra_products_path(category)
    end
    actions defaults: false do |cat|
      unless cat.name.include?("New") || cat.name.include?("Special")
        link_to 'EDIT/DELETE', admin_category_path(cat), class: 'all_btn'
      end
    end
  end

end
