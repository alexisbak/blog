ActiveAdmin.register OrderExtra do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  belongs_to :order_detail
  

  permit_params :order_detail_id, :extra_product_id

  index do
    column 'Product' do |ext|
      ext.order_detail.product.name
    end
    column :extra_product do |ext|
      b ext.extra_product.name
    end
    column 'Price' do |ext|
      b  '$' + ext.extra_product.price.to_s
    end
  end

end
