ActiveAdmin.register Order do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  menu parent: "Order"

  permit_params :user_id, :name, :order_type_id, :status_id, :status, :total, :payment_type_id, :comment, :schedule_date, :schedule_time, :subtotal, :transaction_id, :payment_status
  form do |f|
    f.inputs "Update Order" do
      f.input :name, :input_html => {:disabled => true}
      f.input :status, :as => :select, :collection => Status.select(:id, :name)
      f.input :order_type
      f.input :schedule_date
      f.input :schedule_time
    end
    f.actions
  end

  index do
    column 'Order #' do |order|
      b '#' + order.name
    end
    column :user
    column :order_type do |order|
      if order.order_type.id == 1
        b order.order_type.name, class: 'delivery'
      else
        b order.order_type.name, class: 'pickup'
      end       
    end
    column :payment_type do |order|
       order.payment_type.name.upcase
    end
    column :payment_status do |order|
      status_tag order.payment_status
    end
    column 'Transaction ID', :transaction_id
    column 'Order Status' do |order|
      status_tag order.status.name
    end
    column 'Total' do |order|
      b '$' + order.total.to_s
    end
    column 'Schedule Date' do |order|
      order.schedule_date.upcase + "," + order.schedule_time.upcase
    end
    column :detais do |order|
      link_to 'View', admin_order_order_details_path(order)
    end
    column :comment
    column :created_at
    actions defaults: false do |order|
      link_to t('active_admin.edit'),  edit_admin_order_path(order), class: 'all_btn'
    end
  end

end
