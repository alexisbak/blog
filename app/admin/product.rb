ActiveAdmin.register Product do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  menu parent: "product"
  permit_params :category_id, :name, :image, :desc

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Product" do
      f.input :category
      f.input :name
      f.input :image, :as => :file, :multipart => true, :hint => f.image_tag(f.object.image.url(:med))
      f.input :desc, :type => :text
    end
    f.actions
  end

  index do
    selectable_column
    column :category do |product|
      product.category.name
    end
    column :name do |product|
      b product.name
    end
    column :image do |product|
      image_tag (product.image.url(:small))
    end
    column :desc
    column :details do|product|
      link_to "View Price", admin_product_product_details_path(product)
    end
    actions
  end
  index as: :grid do |product|
    link_to product.name, admin_product_path(product)
    link_to  image_tag (product.image.url(:med))

  end

  index as: :block do |product|
    div for: product do
      resource_selection_cell product
      h2  auto_link     product.name
      div simple_format product.desc
    end
  end

  show do |ad|
    attributes_table do
      row :category
      row :name
      row :image do
        image_tag(ad.image.url(:large))
      end
      row :desc
    end
  end

end
