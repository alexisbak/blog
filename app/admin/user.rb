ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  permit_params :email, :name, :phone, :subscribe, :activated, :activated_at, :reset_sent_at

  show do
    div do
      address = user.addresses.find_by(:default => true)
      div class: 'border' do
        h4 b user.name
        div 'Email: ' + user.email
        div "Phone: " + ActiveSupport::NumberHelper.number_to_phone(address.phone, area_code: true)
        div link_to 'View Addresses',  admin_user_addresses_path(user)
      end
    end
  end

  index do
    column :email
    column :name
    column :address do |user|
      link_to 'View Address', admin_user_addresses_path(user)
    end
    column :subscribe
    column :activated
    column :activated_at
    column :reset_sent_at
  end

end
