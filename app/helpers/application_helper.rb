module ApplicationHelper
  def current_path(path)
    "link_active" if request.url.include?(path)
  end

  def current_path_2(path)
    "menu_active" if request.url.include?(path)
  end

  def is_home
    "menu_active" if request.url == root_url
  end

end
