module CartHelper
  def tax
    return Settings.find(1).value.to_f
  end

  def del_fee
    return Settings.find(2).value.to_f
  end

  def cart
    return nil if cookies[:cart].nil?

    @total = 0;
    @subtotal = 0;
    @del_fee = Settings.find(2).value.to_f
    @product_details = Array.new

    @tax = Settings.find(1).value.to_f

    items = JSON.parse(cookies[:cart])

    items.each do |item|
      temp = Hash.new

      product_detail_obj = ProductDetail.find(item['id'])
      product_obj = Product.find(product_detail_obj.product_id)

      temp["qty"] = item['qty']
      unless product_detail_obj.size.nil?
        temp["size"] = Size.find(product_detail_obj.size_id).name
      else
        temp["size"] = "One Size"
      end

      temp["detail"] = product_detail_obj
      temp["product"] = product_obj

      @subtotal += temp["qty"].to_f * product_detail_obj.price.to_f

      extras = JSON.parse(item['extra'])
      extra_objs = Array.new

      ext_total = 0
      extras.each do |extra_id|
        extra_product = ExtraProduct.find(extra_id)
        ext_total += extra_product.price.to_f
        extra_objs.push(extra_product)
      end
      @subtotal += ext_total*temp["qty"].to_f

      temp["extra"] = extra_objs
      @product_details.push(temp)
    end
    @tax_total = @subtotal* @tax/100
    @tax_total = number_with_precision(@tax_total, precision: 2).to_f
    @total = @tax_total + @subtotal +  @del_fee
    @total = number_with_precision(@total, precision: 2).to_f
  end

  def n_cart
    return '0' if cookies[:cart].nil?
    items = JSON.parse(cookies[:cart])
    qty = 0;
    items.each do |item|
      qty += item["qty"].to_f
    end
    return number_with_precision(qty, precision: 0).to_s
  end

end
