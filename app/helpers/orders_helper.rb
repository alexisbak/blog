module OrdersHelper
  def tax
    return Settings.find(1).value.to_f
  end

  def tax_total(subtotal)
    number_with_precision(subtotal* tax/100, precision: 2).to_f
  end

  def del_fee
    return number_with_precision(Settings.find(2).value, precision: 2).to_f
  end

  def myaddress
    return Settings.find(4).value
  end

  def myzip
    return Settings.find(5).value
  end

  def myphone
    return Settings.find(6).value
  end

  def get_month(i)
    months = Array ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    return months.at(i.to_f - 1)
  end
end
