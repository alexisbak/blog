class Address < ActiveRecord::Base
  before_save :set_def
  belongs_to :user

  validates :address1, :city, :zip, :name, :phone, absence: false

  validates :address1, presence: true
  validates :city, presence: true
  validates :zip, presence: true, length: {is: 5}
  validates :phone, presence: true
  validates_format_of :phone,
      :with => /[0-9]{3}[0-9]{3}[0-9]{4}/,
      :message => "is invalid. Must be xxxxxxxxxxx-format"
  private
  def set_def
    user = User.find(self.user_id)
    if user.addresses.nil? || user.addresses.size == 0
    self.default = true;
    end

    if self.name.nil?
    self.name = user.name
    end

  end
end
