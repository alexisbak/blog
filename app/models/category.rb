class Category < ActiveRecord::Base
  has_many :sizes, dependent: :destroy
  has_many :products, dependent: :destroy
  has_many :extra_products, dependent: :destroy
  has_attached_file :image, styles: { small: "64x64", med: "100x100", large: "200x200" }
  validates_attachment_size :image, :less_than => 5.megabytes
  #validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  self.per_page = 8

end
