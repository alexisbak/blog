class OrderDetail < ActiveRecord::Base
  belongs_to :order
  belongs_to :product_detail
  belongs_to :product
  belongs_to :size
  has_many :order_extras, dependent: :destroy
  before_save {self.product_id = ProductDetail.find(self.product_detail_id).product_id}
  before_save {self.size_id = ProductDetail.find(self.product_detail_id).size_id}

end
