class Size < ActiveRecord::Base
  belongs_to :category
  has_many :product_details
  has_many :order_details
end
