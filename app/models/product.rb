class Product < ActiveRecord::Base
  belongs_to :category, :foreign_key => 'category_id'
  has_many :product_details, dependent: :destroy
  has_many :order_details, dependent: :destroy
  has_attached_file :image, styles: { small: "100x100", med: "300x300", large: "500x500>" }
  validates_attachment_size :image, :less_than => 5.megabytes
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
