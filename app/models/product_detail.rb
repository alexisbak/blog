class ProductDetail < ActiveRecord::Base
  belongs_to :category
  belongs_to :size
  belongs_to :product, :class_name => 'Product', :foreign_key => 'product_id'
  has_many :order_details, dependent: :destroy
end
