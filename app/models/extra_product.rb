class ExtraProduct < ActiveRecord::Base
  belongs_to :category
  has_many :order_extras, dependent: :destroy
end
