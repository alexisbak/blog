require 'yaml'
class Order < ActiveRecord::Base
  before_save :set_def_status
  before_save {self.total = total.to_f }
  after_save :add_name
  belongs_to :user
  belongs_to :order_type
  belongs_to :status, :foreign_key => 'status_id'
  belongs_to :payment_type
  has_many :order_details, dependent: :destroy
  has_many :order_extras
  has_one :card, dependent: :destroy

  accepts_nested_attributes_for :card
  validates :order_type_id, :payment_type_id, :schedule_date, :schedule_time, :subtotal, :total, presence: true

  def send_order_email
    OrderMailer.receipt_email(self).deliver
    OrderMailer.order_notice(self).deliver
  end
  serialize :notification_params, Hash

  def paypal_url(return_path)
    values = {
        business: "caleb6677-facilitator@gmail.com",
        cmd: "_xclick",
        upload: 1,
        return: "#{Rails.application.secrets.app_host}#{return_path}",
        invoice: self.id,
        amount: self.total,
        item_name: 'PrimeGrind Order',
        item_number: '#' + self.name,
        notify_url: "#{Rails.application.secrets.app_host}/hook"
    }
    "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  end
  private

  def add_name
    if self.name.nil?
      name = (user_id + 1000).to_s + "-" + (id + 10000).to_s
      self.update_attribute(:name, name)
    end
  end

  def set_def_status
    if self.status_id.nil?
    self.status_id = 1
    end
  end

end
