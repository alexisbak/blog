class OrderMailer < ActionMailer::Base
  default from: "PrimeGrind <order-confirmation@primegrind.com>"
  helper :orders
  def receipt_email(order)
    @url = 'https://primegrind.herokuapp.com/'
    @user = order.user
    @order = order
    @address = @user.addresses.find_by(:default => true)

    mail(:to => @user.email, :subject => "Your order confirmation: #" + @order.name)
  end

  def order_notice(order)
    to = Settings.find(7).value
    @user = order.user
    @order = order
    @address = @user.addresses.find_by(:default => true)
    mail(:to => to, :subject => "New order #" + @order.name)
  end

end
