class UserMailer < ActionMailer::Base
  default :from => "PrimeGrind <no-reply@primegrind.com>"
  def welcome_email(user)
    @url = 'https://primegrind.herokuapp.com/'
    @user = user
    @categories = Array.new
   
   if Category.all.size() > 7
    for i in 0..5
      category = Category.all[i]
      unless category.products.first.nil?
        @categories.push(Category.all[i])  
      end      
    end 
    if @categories.size() == 6
      mail(:to => user.email, :subject => "Welcome to PrimeGrind")  
    end    
   end    
  end

  def account_activation_email(user)
    @url = 'https://primegrind.herokuapp.com/'
    @user = user
    mail(:to => user.email, :subject => "Account Activation")
  end

  def password_reset_email(user)
    @url = 'https://primegrind.herokuapp.com/'
    @user = user
    mail(:to => user.email, :subject => "Password Reset")
  end
end
