class SessionsController < ApplicationController
  def new
    @title = "Sign In"
    if logged_in?
      redirect_to account_path(:id => @current_user.id)
    end
  end

  def create

    user = User.find_by(:email => params[:session][:email].downcase)

    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        flash[:success] = "Successfully signed in."
        redirect_back_or account_path(:id => user.id)
      else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:info] = message
        redirect_to root_url
      end
    else
      @error = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
