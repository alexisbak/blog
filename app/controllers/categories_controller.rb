class CategoriesController < ApplicationController
  def show
    if Category.where(:id => params[:id]).blank?
        redirect_to root_url
      else
      category = Category.find(params[:id])
      @title = category.name
      @category_name = category.name
      @extraproducts = category.extra_products;
      @products = category.products.order("id DESC")
    end
  end

  def index
    @categories = Array.new
    @title = "Search Results:"

    if (params[:input] != "" && params[:input] != nil)
      value = params[:input]

      Category.all.each do |category|
        products_arr = Array.new
        products = category.products

        products.each do |product|
          if product.name.downcase.include?(value.downcase)
          products_arr.push(product)
          end
        end
        if (products_arr.size() > 0)
          temp = Hash.new
          temp[:cat_name] = category.name
          temp[:products] = products_arr
          temp[:extra_products] = category.extra_products
        @categories.push(temp)
        end
      end
    end
  end

  def search
    redirect_to results_path(:input => search_params[:value])
  end

  private

  def search_params
    params[:search].permit(:value)
  end
end
