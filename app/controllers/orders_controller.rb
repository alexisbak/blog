class OrdersController < ApplicationController

  before_action :is_not_empty, only: [:new, :create]
  before_action :logged_in_user, only: [:new, :create, :show, :index, :receipt]
  before_action :custom_function, only: [:new]
  before_action :corrent_user, only: [:new, :create, :show, :index, :receipt]
  
  def index
    @title = "Order History"
    @size = @current_user.orders.size()
    @orders = @current_user.orders.paginate(:page => params[:page], :per_page => 5).order('id DESC')
    @modal_order = @orders.first
  end

  def new
    @addresses = @current_user.addresses
    @address = @addresses.find_by(:default => true)
  end

  def create
    set_default_address
    @addresses = @current_user.addresses
    @address = @addresses.find_by(:default => true)
    @order = @current_user.orders.build(order_params)
    card_hash = card_params
    card_hash[:card_expires_on] = Date.new(params[:card]["card_expires_on(1i)"].to_i, params[:card]["card_expires_on(2i)"].to_i, params[:card]["card_expires_on(3i)"].to_i)
    @card = @order.build_card(card_hash)
    addDetails @order
  
    
    if @order.save
      case params[:order][:payment_type_id]
      when "1"
          empty_cart
          @order.send_order_email
          flash[:success] = "Successfully checked out."
          redirect_to user_order_path(@current_user, @order)
      when "2"
        if @order.card.purchase
           empty_cart
           @order.send_order_email
          flash[:success] = "Successfully checked out."
          redirect_to user_order_path(@current_user, @order)
        else
          flash[:danger] = @order.card.card_transaction.message
          render 'new'
        end       
      when "3"
        empty_cart
        redirect_to @order.paypal_url(user_order_path(@current_user, @order))
      end       
    else
      render 'new'
    end

  end

  protect_from_forgery except: [:hook]
  def hook
    params.permit! 
    status = params[:payment_status]
    if status == "Completed"
      @order.send_order_email
      @order = Order.find params[:invoice]
      @order.update_attributes payment_status: status, transaction_id: params[:txn_id]
      flash[:success] = "Successfully checked out."    
    end
  end
  
  def show
    if @current_user.orders.where(:id => params[:id]).blank?
      redirect_to root_url     
    else
      @order = @current_user.orders.find(params[:id])
      @address = @current_user.addresses.find_by(:default => true)
      @order_details = @order.order_details      
    end    
      
  end

  def logged_in_user
    unless logged_in?
      store_location
      redirect_to login_path
    end
  end
  
  def corrent_user
    if User.where(:id => params[:user_id]).blank?
        redirect_to root_url
      else
        user = User.find(params[:user_id])
        redirect_to(root_url) unless current_user?(user)         
      end
  end
  
  def is_not_empty
    redirect_to(root_url) if cookies[:cart].nil? || JSON.parse(cookies[:cart]).size == 0
  end

  def empty_cart
    cookies[:cart] = nil
    cookies.delete :cart
  end
     
  def custom_function
    if params[:user_id] == "0"
      redirect_to new_user_order_path(:user_id => @current_user)
    end  
  end
  
  private

  def addDetails order
    unless cookies[:cart].nil?
      items = JSON.parse(cookies[:cart])
      items.each do |item|
        order_detail = order.order_details.build(:product_detail_id => item["id"], :qty => item["qty"])

        extras = JSON.parse(item['extra'])

        extras.each do |extra_id|
          order_detail.order_extras.build(:extra_product_id => extra_id)
        end
      end
    end
  end

  private

  def set_default_address
    addresses = @current_user.addresses
    addresses.each do |address|
      address.update(:default => false)
    end

    address = @current_user.addresses.find(params[:order][:address_id])
    address.update(:default => true)
  end

  private

  def order_params
    params.required(:order).permit(:comment, :order_type_id, :payment_type_id, :total, :schedule_date, :schedule_time, :subtotal)
  end
  
  def card_params
    params.required(:card).permit(:first_name, :last_name, :card_type, :card_number, :card_verification)
  end
end
