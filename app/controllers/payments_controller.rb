class PaymentsController < InheritedResources::Base
  def new
    @payment = Payment.new
  end

  private

  def payment_params
    params.require(:payment).permit(:amount, :responseCode, :responseMessage, :amountReceived, :cardNumberMask,
    :cardType, :cardTypeCode, :responseCode, :transactionID, :xref, :cardNumber,
    :cardCVV, :cardExpiryMonth, :cardExpiryYear)
  end
end

