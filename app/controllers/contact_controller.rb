class ContactController < ApplicationController
  def create
    @message = Message.new(msg_params)

    if @message.valid?
      ContactMailer.contact_email(msg_params).deliver      
    end
  end
  private

  def msg_params
    params[:contact].permit(:name, :email, :content)
  end
end
