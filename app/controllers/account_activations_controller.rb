class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(:email => params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:info] = "Account activated."
      user.send_welcome_email
      redirect_to account_path(:id => user.id)
    else
      flash[:danger] = "Invalid activation link."
      redirect_to root_url
    end
  end
end
