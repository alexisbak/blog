class StaticPagesController < ApplicationController
  def home        
    unless Category.where(:name => "New").blank?
    new_cat = Category.find_by(name: "New")
    @new_products = new_cat.products  
    end
    unless Category.where(:name => "Special").blank?
    special_cat = Category.find_by(name: "Special")
    @special_products = special_cat.products      
    end
    
  end

  def aboutus
    @title = "About Us"

  end

  def contactus
    @title = "Contact Us"
  end

  def create
    @message = Message.new(msg_params)

    if @message.valid?
      ContactMailer.contact_email(msg_params).deliver
    else
      @title = "Contact Us"
      render 'contactus'
    end
  end

  private

  def msg_params
    params[:contact].permit(:name, :email, :content)
  end

end
