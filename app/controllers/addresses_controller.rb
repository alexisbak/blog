class AddressesController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :update, :show]
  before_action :correct_user,   only: [:new, :create, :edit, :update, :show]
  def new
    @title = "New Address"
    @address = @current_user.addresses.build
  end

  def create
    @address = @current_user.addresses.build(address_params)

    if @address.save
      flash[:success] = "Successfully added."
      redirect_to account_path(:id => @current_user.id)
    else
      render 'new'
    end
  end

  def edit
    @title = "Update Address"
    @user = User.find(params[:user_id])
    @address = @user.addresses.find(params[:id])
  end

  def update
    @user = User.find(params[:user_id])
    @address = @user.addresses.find(params[:id])
    
    if @address.update(address_params)
      flash[:success] = "Successfully updated."
      redirect_to account_path(:id => @user.id)
    else
      render 'edit'
    end   
    
  end

  def destroy
    @address = Address.find(params[:id])
    if @address.default == true
      flash[:danger] = "Default address can not be deleted."
    else
      @address.destroy
      flash[:success] = "Successfully deleted."
    end
    redirect_to account_path(:id => params[:user_id])
  end

  def logged_in_user
    unless logged_in?
      store_location
      redirect_to login_path
    end
  end

  def correct_user
    @user = User.find(params[:user_id])
    redirect_to(root_url) unless current_user?(@user)
  end

  private

  def address_params
    params.require(:address).permit(:address1, :address2, :city, :zip, :name, :phone)
  end
end
