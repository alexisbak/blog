class UsersController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_action :logged_in_user, only: [:edit, :update, :show]
  before_action :correct_user,   only: [:edit, :update, :show]

  def new
    @title = "Sign Up"
    redirect_to account_path(:id => @current_user.id) if logged_in?

    @user = User.new
    @address = @user.addresses.build
  end

  def create
    @user = User.new(user_params)
    @address = @user.addresses.build(address_params)

    if @user.save
      #log_in @user
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end

  def show
    @title = "My Account"
    if @current_user.addresses.find_by(:default => true).nil?
      @address = @current_user.addresses.first
      @address.update(:default => true)
    end
    @phone = @current_user.addresses.find_by(:default => true).phone

  end

  def edit
    @title = "Update My Profile"
    @user = @current_user
  end

  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      flash[:success] = "Successfully Updated."
      redirect_to account_path(:id => @user.id)
    else
      render 'edit'
    end

  end

  def logged_in_user
    unless logged_in?
      store_location
      redirect_to login_path
    end
  end

  def correct_user
    if User.where(:id => params[:id]).blank?
      redirect_to root_url
    else
      user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(user)
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :name, :password, :password_confirmation, :subscribe)
  end

  private

  def address_params
    params[:user].require(:address).permit(:address1, :address2, :city, :zip, :phone)
  end

end
