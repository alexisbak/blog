class ProductsController < ApplicationController
  def show
    if Product.where(:id => params[:id]).blank?
      redirect_to root_path
    else      
      @product = Product.find(params[:id])
      @extraproducts = @product.category.extra_products
    end
  end
end
