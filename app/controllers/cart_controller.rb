class CartController < ApplicationController
  helper :orders
  before_action :logged_in_user, only: [:set]
  before_action :correct_user, only: [:set]
  def index
    @title = "Cart"
    if logged_in?
      @user_id = @current_user.id
    end
  end

  def set
    order = @current_user.orders.find(params[:id])
    order_details = order.order_details

    items = Array.new
    order_details.each do |detail|
      temp = Hash.new
      temp["id"] = detail.product_detail_id
      temp["qty"] = detail.qty

      extras = Array.new
      detail.order_extras.each do |extra|

        extras.push(extra.extra_product_id)
      end
      temp["extra"] = ActiveSupport::JSON.encode(extras)

      items.push(temp)
    end
    cookies[:cart] = ActiveSupport::JSON.encode(items)
    flash[:success] = "Successfully added to cart."
    redirect_to :action => :index
  end

  def logged_in_user
    unless logged_in?
      store_location
      redirect_to login_path
    end
  end

  def correct_user
    @user = User.find(params[:user_id])
    redirect_to(new_user_order_path(:user_id => @current_user.id)) unless current_user?(@user)
  end
end
