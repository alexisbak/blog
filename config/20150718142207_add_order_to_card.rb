class AddOrderToCard < ActiveRecord::Migration
  def change
    add_reference :cards, :order, index: true
  end
end
