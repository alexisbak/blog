Rails.application.routes.draw do

  resources :registrations

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'welcome/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'static_pages#home'

  
  get 'static_pages/aboutus'
  get 'static_pages/contactus'
  post 'static_pages/create'
  get 'password_resets/new'
  get 'password_resets/edit'
  get 'sessions/new'
  post 'categories/search'
  get 'categories/index'

  get 'contact' => 'static_pages#contactus'
  get 'aboutus' => 'static_pages#aboutus'
  post 'contact' => 'static_pages#create'
  post 'results' => 'categories#search'
  get 'results' => 'categories#index'
  resources :categories
  resources :products

  resources :cart do
    collection do
      get 'checkout'
      get 'set'
    end
  end

  resources :users do
    resources :addresses
    resources :orders
  end

  resources :payments

  get 'signup'  => 'users#new'
  get 'account' => 'users#show'
  get 'update' => 'users#edit'

  post "/hook" => "orders#hook"
  post "/users/:user_id/orders/:id" => 'orders#index'

  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  resources :contact, only: [:create]
  resources :account_activations, only: [:edit]
  resources :password_resets, only: [:new, :create, :edit, :update]

# Example of regular route:
#   get 'products/:id' => 'catalog#view'

# Example of named route that can be invoked with purchase_url(id: product.id)
#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

# Example resource route (maps HTTP verbs to controller actions automatically):
#   resources :products

# Example resource route with options:
#   resources :products do
#     member do
#       get 'short'
#       post 'toggle'
#     end
#
#     collection do
#       get 'sold'
#     end
#   end

# Example resource route with sub-resources:
#   resources :products do
#     resources :comments, :sales
#     resource :seller
#   end

# Example resource route with more complex sub-resources:
#   resources :products do
#     resources :comments
#     resources :sales do
#       get 'recent', on: :collection
#     end
#   end

# Example resource route with concerns:
#   concern :toggleable do
#     post 'toggle'
#   end
#   resources :posts, concerns: :toggleable
#   resources :photos, concerns: :toggleable

# Example resource route within a namespace:
#   namespace :admin do
#     # Directs /admin/products/* to Admin::ProductsController
#     # (app/controllers/admin/products_controller.rb)
#     resources :products
#   end
end
