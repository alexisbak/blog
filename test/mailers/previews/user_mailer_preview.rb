# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def account_activation
    user = User.first
    user.activation_token = User.new_token
    UserMailer.account_activation_email(user)
  end

  def welcome
    user = User.first
    UserMailer.welcome_email(user)
  end

  def reset
    user = User.find(22)
    user.reset_token = User.new_token
    UserMailer.password_reset_email(user)
  end
end
