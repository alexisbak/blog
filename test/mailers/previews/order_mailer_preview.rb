# Preview all emails at http://localhost:3000/rails/mailers/order_mailer
class OrderMailerPreview < ActionMailer::Preview
  def new_order
    user = User.find(22)
    order = user.orders.first
    OrderMailer.order_notice(order)
  end
  
  def receipt
    user = User.find(22)
    order = user.orders.first
    OrderMailer.receipt_email(order)
  end

end
