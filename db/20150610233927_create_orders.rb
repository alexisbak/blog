class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.references :user, index: true
      t.references :order_type, index: true
      t.references :status, index: true
      t.decimal :total

      t.timestamps
    end
  end
end
