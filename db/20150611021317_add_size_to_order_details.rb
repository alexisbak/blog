class AddSizeToOrderDetails < ActiveRecord::Migration
  def change
    add_reference :order_details, :size, index: true
  end
end
