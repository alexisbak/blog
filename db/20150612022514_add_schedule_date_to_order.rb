class AddScheduleDateToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :schedule_date, :string
  end
end
