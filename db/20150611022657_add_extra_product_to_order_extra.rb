class AddExtraProductToOrderExtra < ActiveRecord::Migration
  def change
    add_reference :order_extras, :extra_product, index: true
  end
end
