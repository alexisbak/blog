class CreateOrderDetails < ActiveRecord::Migration
  def change
    create_table :order_details do |t|
      t.references :order, index: true
      t.references :product_detail, index: true
      t.integer :qty

      t.timestamps
    end
  end
end
