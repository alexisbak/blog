class CreateSize < ActiveRecord::Migration
  def change
    create_table :sizes do |t|
      t.references :category, index: true
      t.string :name
    end
  end
end
