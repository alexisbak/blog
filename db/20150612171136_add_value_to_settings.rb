class AddValueToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :value, :string
  end
end
