class CreateProduct < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.references :category, index: true
      t.string :name
      t.attachment :image
      t.text :desc
    end
  end
end
