class AddScheduleTimeToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :schedule_time, :string
  end
end
