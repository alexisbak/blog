class RemoveValueFromSettings < ActiveRecord::Migration
  def change
    remove_column :settings, :value, :integer
  end
end
