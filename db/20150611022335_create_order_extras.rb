class CreateOrderExtras < ActiveRecord::Migration
  def change
    create_table :order_extras do |t|
      t.references :order_detail, index: true

      t.timestamps
    end
  end
end
