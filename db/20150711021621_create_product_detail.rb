class CreateProductDetail < ActiveRecord::Migration
  def change
    create_table :product_details do |t|
      t.references :category, index: true
      t.references :product, index: true
      t.references :size, index: true
      t.decimal :price
    end
  end
end
