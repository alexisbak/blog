class CreateAddress < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :user, index: true
      t.string :name
      t.string :address1
      t.string :address2
      t.string :city
      t.integer :zip
      t.string :phone
      t.boolean :default
    end
  end
end
