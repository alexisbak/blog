class CreateExtraProduct < ActiveRecord::Migration
  def change
    create_table :extra_products do |t|
      t.references :category, index: true
      t.string :name
      t.decimal :price
    end
  end
end
